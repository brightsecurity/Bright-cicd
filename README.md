1. Create a new branch
2. Create API key at Nexploit.app -> User Settings -> Create new API Key -> Choose name and Select All and click Create
3. Set NEXPLOIT_TOKEN and REPEATER (if needed) at Settings -> CI/CD -> Variables
4. Run a CI job in Actions.
5. Go to Nexploit app and check if a scan started..
